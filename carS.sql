-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Feb 25, 2019 at 11:51 AM
-- Server version: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cardb`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `aid` int(10) UNSIGNED NOT NULL,
  `uid` int(11) NOT NULL,
  `vehicle_make` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` enum('express_maintenance','express_repairs','heavy_repairs') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'express_maintenance',
  `appointment_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appointment_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`aid`, `uid`, `vehicle_make`, `category`, `appointment_date`, `appointment_time`, `name`, `email`, `phone`, `message`, `remember_token`, `created_at`, `updated_at`, `sid`) VALUES
(7, 1, 'Toyota', 'express_maintenance', '28/02/2019', 'Morning', 'another test', 'parley@ship.com', '08120522261', 'vdfsgsdfd', NULL, '2019-02-25 10:04:21', '2019-02-25 10:04:21', NULL),
(9, 1, 'Toyota', 'express_maintenance', '28/02/2019', 'Morning', 'asar sunny', 'ring2rings93@gmail.com', '08120522261', 'sads', NULL, '2019-02-25 10:43:09', '2019-02-25 10:43:09', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`aid`),
  ADD UNIQUE KEY `appointments_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `aid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
