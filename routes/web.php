<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/','PublicController@index');

Auth::routes();

// Admin Route


Route::group(['middleware' => 'admin'], function(){
            Route::get('/home', 'HomeController@index')->name('home');

            // Route::get('/dashboard','HomeController@dashboard');

            Route::get('logout','HomeController@logout');

            Route::get('service', 'HomeController@getService');
            Route::post('service', 'HomeController@postService');
            Route::get('vehicle', 'HomeController@getVehicle');
            Route::post('vehicle', 'HomeController@postVehicle');

            Route::get('add-staff', 'HomeController@getAddStaff');
            Route::post('add-staff', 'HomeController@postAddStaff');

            Route::get('manage-staff', 'HomeController@manageStaff');
            
            Route::get('edit-staff/{stid}', 'HomeController@editStaff');
            Route::post('post-edit-staff/{stid}', 'HomeController@postEditStaff');
            Route::get('delete-staff/{stid}', 'HomeController@deleteStaff');

            Route::get('view-staff-detail/{stid}', 'HomeController@viewStaffDetails');

            Route::get('appointment-data','HomeController@manageAppointmentData');
            Route::get('/appointment-data-details/{ahid}', 'HomeController@viewAppointmentDataDetails');
            Route::get('delete-appointment-data/{ahid}', 'HomeController@deleteAppointmentData');

            //designations
            Route::get('add-designation','HomeController@addDesignation');
            Route::post('post-designation','HomeController@postDesignation');
            Route::get('designations','HomeController@designations');

            Route::get('designation/{did}/edit','HomeController@editDesignation');
            Route::get('designation/{did}/delete','HomeController@deleteDesignation');

            Route::post('designation/{did}/edit','HomeController@postEditDesignation');


});


Route::group(['middleware' => 'customer'], function(){
// book appointment Routes

Route::get('/book-appointment', 'CustomerController@bookAppointment');
Route::post('/book-appointment', 'CustomerController@postBookAppointment');
});

//Authentication for staff
Route::get('staff/login','AuthenticationController@loginStaff');
Route::post('staff/login','AuthenticationController@postLoginStaff');
Route::get('staff/dashboard','StaffController@dashboard');

//Authentication for customer
Route::get('register-customer', 'PublicController@registerCustomer');
Route::post('post-register-customer', 'PublicController@postRegisterCustomer');

Route::get('customer/login','AuthenticationController@loginCustomer');
Route::post('customer/login','AuthenticationController@postLoginCustomer');

//you have to do something about this dashboard link below
Route::get('customer/dashboard','CustomerController@dashboard');

Route::get('/profile/{cuid}','CustomerController@getProfile');
Route::get('edit-customer/{cuid}', 'CustomerController@editCustomer');
Route::post('post-edit-customer/{cuid}', 'CustomerController@postEditCustomer');


Route::get('/change-password', 'HomeController@changePassword');
Route::post('/change-password','HomeController@postChangePassword');

Route::get('/changeCustomerPassword', 'CustomerController@getChangeCustomerPassword');
Route::post('/changeCustomerPassword','CustomerController@postChangeCustomerPassword');
//manage appointment routes


// arrange

// Route::get('staff/manage-appointment', 'HomeController@manageAppointment');
Route::get('staff/manage-bookings', 'StaffController@manageBookings');
Route::get('staff/appointment-details/{aid}', 'StaffController@viewAppointmentDetails');
Route::get('staff/delete-appointment/{aid}', 'StaffController@deleteAppointment');
Route::get('staff/process-appointment/{aid}', 'StaffController@processAppointment');
Route::get('staff/complete-appointment/{aid}', 'StaffController@completeAppointment');



Route::get('logout-user/{user}','AuthenticationController@logoutUser');



Route::get('/feedback', 'CustomerController@getFeedback');
Route::post('/feedback', 'CustomerController@postFeedback');


Route::get('/contact', 'PublicController@contact');

//public routes
Route::get('/about', 'PublicController@about');


//left with no export

// Todo:
//finish feedback
//finish the front end
// Finish the management 
//finish the staff
//finish the import
//finish export




