<!--  footer -->
  <footer id="footer" class="footer-main-block">
   {{-- Footer block was here --}}
    <hr>
    <div class="copyright">
      <div class="container">
        <div class="row">
          
          <div class="col-md-8">
            <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                <img src="{{ url('frontend/images/logo.png') }}" alt="">   
                 
                  <li class="nav-item" id="aboutus">
                    <p>Toyota Ghana Company Limited is the ONLY company in Ghana authorized by the Toyota Manufacturer (Toyota Motors Corporation) under a signed authorized distributorship agreement to distribute durable TOYOTA VEHICLES, Genuine SPARE PARTS and the provision of Quality <span><a href="#" class="nav-link" target="_blank">AFTER-SALES SERVICES.</a></span></p>
                  </li>
                  
                  
               
                </ul>
          </div>
          <div class="col-md-2">
              <ul class="nav nav-footer justify-content-center justify-content-xl-end">
               
                <li class="nav-item">
                  <a href="#" class="nav-link" target="_blank">About Us</a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link" target="_blank">Feedback</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" target="_blank">Contact Us</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" target="_blank">More</a>
                </li>
             
              </ul>
          </div>

          
          <div class="col-md-2">
              <ul class="nav nav-footer justify-content-center justify-content-xl-end">
               
                <li class="nav-item">
                  <a href="{{ url('staff/login') }}" class="nav-link">Staff Portal</a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('login') }}" class="nav-link">Admin</a>
                </li>
 
             
              </ul>
          </div>

          <div class="col-md-12">
              <div class="copyright-text text-center">
                <p>&copy; Copyrights <?php echo date('Y') ?> <a href="index.html">Toyota Ghana Limited</a>. | All Rights Reserved.</p>
              </div>
            </div>

        </div>
      </div>
    </div>
  </footer>
<!--  end footer -->