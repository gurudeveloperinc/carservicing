<!--  page banner -->
  <div id="page-banner" class="page-banner-main" style="background:grey;')">
    {{-- style="background-image: url('frontend/images/bg/page-banner.jpg')" --}}
    <div class="container">
      <div class="page-banner-block">
        <div class="section">
          <h3 class="section-heading">Appointments</h3>
        </div>
        <ol class="breadcrumb">
          <li><a href="{{ url('/') }}">Home</a></li>
          <li><a href="#">Page</a></li>
          <li class="active"><a>Appointments</a></li>
        </ol>
      </div>
    </div>
  </div>
<!--  end page banner  -->