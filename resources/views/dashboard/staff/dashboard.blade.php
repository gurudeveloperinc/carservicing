@extends('dashboard.staff.layouts.staff')

@section('content')
<style>
    .bg-gradient-primary{
        background: linear-gradient(87deg, #525f7fad 0, #172b4d 100%) !important;
    }
</style>

 @include('dashboard.staff.main.top') 

<!-- Header -->
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
  <div class="container-fluid">
    <div class="header-body">
      <!-- Card stats -->
      <div class="row">
        <div class="col-xl-3 col-lg-6">
          <div class="card card-stats mb-4 mb-xl-0">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Successful Transactions</h5>
                  <span class="h2 font-weight-bold mb-0">{{ $appointments }}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                    <i class="fas fa-chart-bar"></i>
                  </div>
                </div>
              </div>
          
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6">
          <div class="card card-stats mb-4 mb-xl-0">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Pending Transactions</h5>
                  <span class="h2 font-weight-bold mb-0">{{ $pendingAppointments }}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-warning text-white rounded-circle shadow">
                    <i class="fas fa-chart-pie"></i>
                  </div>
                </div>
              </div>
            
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6">
          <div class="card card-stats mb-4 mb-xl-0">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Transactions In Progress</h5>
                  <span class="h2 font-weight-bold mb-0">{{ $appointment }}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                    <i class="fas fa-users"></i>
                  </div>
                </div>
              </div>
           
            </div>
          </div>
        </div>
        <div class="col-xl-3 col-lg-6">
          <div class="card card-stats mb-4 mb-xl-0">
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <h5 class="card-title text-uppercase text-muted mb-0">Customers</h5>
                  <span class="h2 font-weight-bold mb-0">{{ count($customers) }}</span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                    <i class="fas fa-percent"></i>
                  </div>
                </div>
              </div>
           
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
















{{--  

<ul>
        <li><a style="color:black !important;" href="{{url('change-password')}}"><i class="ti-lock"></i> <span>Change Password</span></a></li>
        @if(session()->has('staff'))
            @php(  $user = session()->get('staff'))
            <li><a style="color:black !important;" href="{{url('logout-user/'.$user->role)}}"><i class="ti-power-off"></i> <span>Logout</span></a></li>

        @endif
    </ul>  --}}