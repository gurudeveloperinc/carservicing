@extends('dashboard.customer.layouts.auth')

@section('content')

<style type="text/css">
.loglink{
    color: #ffffff;
    text-decoration: none !important;
    font-size: 12px;
}
.loglink:hover{
    color:#ffffff;
}

.loglink h6{
    font-size: 12px;
}
    
</style>

    <div class="col-sm-2 col-md-3 col-lg-6 col-xl-8 d-none d-md-block">
                    <div class="side-1"></div>
                    <div class="side-1-overlay"></div>
                    <div class="info-side">
                        <a class="loglink" href="{{ url('/') }}">
                            <h1>Toyota Gh</h1>
                            <h6>Authorized by the Toyota Manufacturer (Toyota Motors Corporation) </h6>               
                        </a>
                        <div class="main-info">
                            
                            <h1>Wecome to Customer Portal</h1>
                            <h6> It is our pleasure to provide you with the best vehicle purchase and <br>ownership experience. </br></h6>
                            <a href="{{ url('/') }}">
                            <button class="btn btn-primary ob-btn btn-light" type="button">Learn more</button><button class="btn btn-primary ob-btn btn-flat" type="button" >Go Back</button>
                            </a>
                        </div>
                        <!-- the span class below has a Home link caption -->
                                
                        <div
                            class="bottom"><span></span><span><a class="loglink" href="{{ url('/') }}"> <?php echo date ('Y') ?> Developed By Abdul Sheriff</a></span>
                        </div>
                    </div>
    </div>
    <div class="col">
        <div class="login-container">
            @include('notification')
            <a href="{{ url('/') }}"><img src="{{ url('backend/assets/img/cdd_logo.png') }}" ></a>
            <h6 class="sub-title"><br>Please login to your <br>dashboard<br><br></h6>
            <form id="login-form" role="form" method="POST" action="{{ url('customer/login')}}">

                {{ csrf_field() }}

                @if ($errors->has('email'))
                    <div class="error" align="center">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif

          
                <div class="form-group input-field s12 {{ $errors->has('email') ? ' has-error' : '' }}"><label>Email<br></label>
                    <input class="form-control validate" id="email" type="text" name="email" value="{{ old('email') }}" required autofocus>
                </div>

                @if ($errors->has('password'))
                <span class="error">
                            <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

                <div class="form-group input-field s12  {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Password</label>
                    <input class="form-control" id="password" type="password" name="password" required>
                </div>
                <div class="col-md-6 col-md-offset-4">
                    <input type="checkbox" id="remember" name="remember" />
                    <label for="remember">Remember Me</label>
                </div>
                <p id="fgt-pass"><a href="{{ url('/password/reset') }}">Forgot Password?</a></p>
                

                <div class="btn-cont"><button class="btn btn-primary ob-btn btn-dark" type="submit">Login </button></div>
            </form>
            <div class="bottom">
                <p>You don't have account? Kindly contact <span> <a href="#" style="color: red !important; "> IT Unit</a> to proceed</span><br></p>
            </div>

            
        </div>
    </div>






@endsection


















