@extends('layouts.app')


@section('content')
<style>
  
  textarea[class=mymessage] {
    resize: none;
    min-width: 100%;
  }
</style>

<!--  appointments -->
  <div id="appointments" class="appointment-main-block appointment-two-main-block">
    <div class="container">
      <div class="row">
          @include('notification')
        <div class="section text-center">
          <h3 class="section-heading text-left">Edit Information</h3>
        </div>
       
 
  
        <div class="col-md-8 col-sm-12">
          <div class="appointment-block">
            <form id="appointment-form" class="appointment-form" method="post" enctype="multipart/form-data" action="{{url('post-edit-customer/'.$customer->cuid)}}">
                {{csrf_field()}}

              <h5 class="form-heading-title"><span class="form-heading-no"><i class="fa fa-user"></i> </span>Profile Information</h5>
              <div class="row">
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="name" name="name" value="{{ $customer->name }}" required>
                </div>
                <div class="col-sm-4">
                  <input type="email" class="form-control" id="email" name="email" value="{{ $customer->email }}" required>
                </div>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="phone" name="phone" value="{{ $customer->phone }}" required>
                </div>
                <div class="col-sm-12">
                  <textarea name="address" class="mymessage" rows="4" cols="110">{{ $customer->address }}</textarea>
                </div>
              </div>

            
              <button type="submit" class="btn btn-default pull-right">Save Changes</button>
            </form>
          </div>
        </div>

       

      </div>
    </div>
  </div>
<!--  end appointments -->

@include('main.footer')


@endsection




