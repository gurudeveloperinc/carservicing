<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>Toyota Gh</title>

    <meta name="description" content="Toyota Car" />
    <meta name="keywords" content="Welcome to Toyota company Ghana limited">
    <meta name="author" content="Jonah Boyi" />
    <meta name="MobileOptimized" content="320" />
    <link rel="icon" type="image/icon" href="{{ url('./backend/assets/img/brand/logo.png') }}"> <!-- favicon-icon -->
    <!-- theme style -->
    <link href="{{ url('frontend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/> <!-- bootstrap css -->
    <link href="{{ url('frontend/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/> <!-- fontawesome css -->
    <link href="{{ url('frontend/css/icon-font.css') }}" rel="stylesheet" type="text/css"/> <!-- icon-font css -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,700|Poppins:400,500,700" rel="stylesheet"> <!-- google font -->
    <link href="{{ url('frontend/css/menumaker.css') }}" rel="stylesheet" type="text/css"/> <!-- menu css -->
    <link href="{{ url('frontend/css/owl.carousel.css') }}" rel="stylesheet" type="text/css"/> <!-- owl carousel css -->
    <link href="{{ url('frontend/css/magnific-popup.css') }}" rel="stylesheet" type="text/css"/> <!-- magnify popup css -->
    <link href="{{ url('frontend/css/datepicker.css') }}" rel="stylesheet" type="text/css"/> <!-- datepicker css -->
    <link href="{{ url('frontend/css/style.css') }}" rel="stylesheet" type="text/css"/> <!-- custom css -->
    <link href="{{ url('frontend/css/checkbox.css') }}" rel="stylesheet" type="text/css"/> <!-- custom css -->

    <style>
        .alignmenu{
            display: flex;
            justify-content: space-between;
        }

        @media only screen and (min-width: 425px) and (max-width: 1500px) {
        .mainnavs{
        }
        .logo img{
            width: 150px ;
        }
        }
    </style>
</head>
<body data-gr-c-s-loaded="true">
        <!-- preloader --> 
        <div class="preloader">
          <div class="status">
            <div class="status-message">
            </div>
          </div>
        </div>
      <!-- end preloader --> 

      



    <div id="app">

        <main class="py-4">
            @yield('content')
        </main>
    </div>


    <!-- jquery -->
    <script type="text/javascript" src="{{ url('frontend/js/jquery.min.js') }}"></script> <!-- jquery library js -->
    <script type="text/javascript" src="{{ url('frontend/js/bootstrap.min.js') }}"></script> <!-- bootstrap js -->
    <script type="text/javascript" src="{{ url('frontend/js/owl.carousel.js') }}"></script> <!-- owl carousel js -->
    <script type="text/javascript" src="{{ url('frontend/js/jquery.ajaxchimp.js') }}"></script> <!-- mail chimp js -->
    <script type="text/javascript" src="{{ url('frontend/js/smooth-scroll.js') }}"></script> <!-- smooth scroll js -->
    <script type="text/javascript" src="{{ url('frontend/js/jquery.magnific-popup.min.js') }}"></script> <!-- magnify popup js --> 
    <script type="text/javascript" src="{{ url('frontend/js/waypoints.min.js') }}"></script> <!-- facts count js required for jquery.counterup.js file -->
    <script type="text/javascript" src="{{ url('frontend/js/jquery.counterup.js') }}"></script> <!-- facts count js-->
    <script type="text/javascript" src="{{ url('frontend/js/menumaker.js') }}"></script> <!-- menu js--> 
    <script type="text/javascript" src="{{ url('frontend/js/jquery.appear.js') }}"></script> <!-- progress bar js -->
    <script type="text/javascript" src="{{ url('frontend/js/jquery.countdown.js') }}"></script>  <!-- event countdown js -->
    <script type="text/javascript" src="{{ url('frontend/js/price-slider.js') }}"></script> <!-- price slider / filter js-->
    <script type="text/javascript" src="{{ url('frontend/js/bootstrap-datepicker.js') }}"></script> <!-- bootstrap datepicker js--> 
    <script type="text/javascript" src="{{ url('frontend/js/jquery.elevatezoom.js') }}"></script> <!-- image zoom js-->
    <script type="text/javascript" src="{{ url('frontend/js/theme.js') }}"></script> <!-- custom js -->  
    <!-- end jquery -->
</body>
</html>
