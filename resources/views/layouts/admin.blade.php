<!DOCTYPE html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Welcome to Toyota Ghana Branch">
    <meta name="author" content="Creative Tim">
    <title>Toyota Gh - Dashboard</title>
    <!-- Favicon -->
    <link href="{{ url('./backend/assets/img/brand/logo.png') }}" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="{{ url('../backend/assets/vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
    <link href="{{ url('../backend/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="{{ url('../backend/assets/css/argon.css') }}" rel="stylesheet">

    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> --}}
    {{-- <script src="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"> --}}

    <!-- DataTables -->
    <link href="{{ url('backend/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ url('backend/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ url('backend/datatables/buttons.dataTables.css') }}" rel="stylesheet" type="text/css">



  
    <style>
      .newlist{
        list-style-type: none;
      }

      .navcardhead{
        padding-left: 0% !important;
      }
      
      .card-body p{
        font-size: 14px;
      }
    </style>
</head>
<body>
    <!-- Sidenav -->
    <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
      <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="./index.html">
          <img src="{{ url('./frontend/images/logo.png') }}" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
          <li class="nav-item dropdown">
            <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="ni ni-bell-55"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Something else here</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="./assets/img/theme/team-1-800x800.jpg">
                </span>
              </div>
            </a>
            <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
              <div class=" dropdown-header noti-title">
                <h6 class="text-overflow m-0">Welcome!</h6>
              </div>
              {{-- <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a> --}}
             
              {{--  <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-settings-gear-65"></i>
                <span>Settings</span>
              </a>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-calendar-grid-58"></i>
                <span>Activity</span>
              </a>
              <a href="./examples/profile.html" class="dropdown-item">
                <i class="ni ni-support-16"></i>
                <span>Support</span>
              </a>  --}}
             
              <div class="dropdown-divider"></div>
              <a href="{{ url('logout') }}" class="dropdown-item">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
               
              </a>
            </div>
          </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Collapse header -->
          <div class="navbar-collapse-header d-md-none">
            <div class="row">
              <div class="col-6 collapse-brand">
                <a href="./index.html">
                  <img src="./assets/img/brand/blue.png">
                </a>
              </div>
              <div class="col-6 collapse-close">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                  <span></span>
                  <span></span>
                </button>
              </div>
            </div>
          </div>
          <!-- Form -->
          <form class="mt-4 mb-3 d-md-none">
            <div class="input-group input-group-rounded input-group-merge">
              <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <span class="fa fa-search"></span>
                </div>
              </div>
            </div>
          </form>
          <!-- Navigation -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/home') }}">
                <i class="ni ni-tv-2 text-primary"></i> Dashboard
              </a>
            </li>             
            
            <li class="nav-item">
              <a class="nav-link" href="{{ url('/vehicle') }}">
                <i class="ni ni-key-25 text-info"></i> Vehicle Makes
              </a>
            </li>
            {{--  try and see if you can be able to send notifications to customers   --}}
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/service') }}">
                  <i class="ni ni-planet text-blue"></i> Services
                </a>
              </li>

            {{-- <li class="nav-item">
              <a class="nav-link" href="./examples/register.html">
                <i class="ni ni-circle-08 text-pink"></i> Feedbacks
              </a>
            </li> --}}

          </ul>
          <!-- Divider -->
          <hr class="my-3">
          {{--  feedback was here  --}}

          <div role="tablist" id="accordion-2">

              <div class=" nav-link navcard"> 
                  
                      <div class="card-header navcardhead" role="tab">
                          <h5 class="mb-0"><a data-toggle="collapse" aria-expanded="true" aria-controls="accordion-2 .item-2" href="div#accordion-2 .item-2"> <i class="ni ni-bullet-list-67 text-info mr-3"></i> <span>Appointments</span></a></h5>
                      </div>
                      <div class="collapse item-2" role="tabpanel" data-parent="#accordion-2">
                          <div class="card-body">
                              {{--  <p class="card-text"><a href="{{ url('/home') }}">Add</a></p>  --}}
                              <p class="card-text"><a href="{{ url('/appointment-data') }}">All Transactions</a></p>
                          </div>
                      </div>
              </div>
            </div>

            <div role="tablist" id="accordion-3">

                <div class=" nav-link navcard">
                    
                        <div class="card-header navcardhead" role="tab">
                            <h5 class="mb-0"><a data-toggle="collapse" aria-expanded="true" aria-controls="accordion-3 .item-3" href="div#accordion-3 .item-3"> <i class="ni ni-single-02 text-yellow mr-3"></i> <span>Users/Staff</span></a></h5>
                        </div>
                        <div class="collapse item-3" role="tabpanel" data-parent="#accordion-3">
                            <div class="card-body">
                                <p class="card-text"><a href="{{ url('/add-staff') }}">Add</a></p>
                                <p class="card-text"><a href="{{ url('/manage-staff') }}">Manage</a></p>
                            </div>
                        </div>
                </div>
              </div> 

              <div role="tablist" id="accordion-4">

                  <div class=" nav-link navcard"> 
                      
                          <div class="card-header navcardhead" role="tab">
                              <h5 class="mb-0"><a data-toggle="collapse" aria-expanded="true" aria-controls="accordion-4 .item-4" href="div#accordion-4 .item-4"> <i class="fas fa-chart-pie text-blue mr-3"></i> <span>Reports</span></a></h5>
                          </div>
                          <div class="collapse item-4" role="tabpanel" data-parent="#accordion-4">
                              <div class="card-body">
                                  {{--  <p class="card-text "><a href="{{ url('/home') }}">View</a></p>
                                  <p class="card-text"><a href="{{ url('/home') }}">Generate Invoice</a></p>
                                  <p class="card-text"><a href="{{ url('/home') }}">Import</a></p>  --}}
                                  <p class="card-text"><a href="{{ url('/home') }}">Export Data</a></p>

                              </div>
                          </div>
                  </div>
                </div>
          
          {{-- <!-- Heading -->
          <h6 class="navbar-heading text-muted">Documentation</h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html">
                <i class="ni ni-spaceship"></i> Getting started
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/foundation/colors.html">
                <i class="ni ni-palette"></i> Foundation
              </a>
            </li> --}}
            <li class="nav-item newlist">
              <a class="nav-link" href="{{ url('logout') }}">
                <i class="ni ni-ui-04"></i> Logout
              </a>
            </li>
          {{-- </ul> --}}

        

       

        </div>
      </div>
    </nav>   


    {{-- <div id="app">

        <main class="py-4">
        </main>
      </div> --}}

      <div class="main-content">

      @yield('content')
     
      
    </div>

     



    <!-- Argon Scripts -->
    <!-- Core -->
    <script src="{{ url('./backend/assets/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ url('./backend/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Optional JS -->
    <script src="{{ url('./backend/assets/vendor/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ url('./backend/assets/vendor/chart.js/dist/Chart.extension.js') }}"></script>
    <!-- Argon JS -->
    <script src="{{ url('./backend/assets/js/argon.js?v=1.0.0') }}"></script>

    <!-- Datatables-->
    <script src="{{ url('backend/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('backend/datatables/dataTables.responsive.min.js') }}"></script>
    
    <script src="{{ url('backend/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ url('backend/datatables/jszip.min.js') }}"></script>
    <script src="{{ url('backend/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ url('backend/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ url('backend/datatables/buttons.html5.min.js') }}"></script>
      <script>
        {{-- $(document).ready(function () {
            $('#datatable1').dataTable();
        }); --}}


            $(document).ready(function () {
              $('#datatable1').DataTable({
                  dom: 'Bfrtip',
                  buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                  ]
                });

          });
    </script>

    {{-- $(document).ready(function() {
      $('#example').DataTable( {
          dom: 'Bfrtip',
          buttons: [
              'copyHtml5',
              'excelHtml5',
              'csvHtml5',
              'pdfHtml5'
          ]
      } );
  } ); --}}



  
</body>
</html>