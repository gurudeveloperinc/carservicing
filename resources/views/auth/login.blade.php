








@extends('layouts.auth')

@section('content')

<style type="text/css">
.loglink{
    color: #ffffff;
    text-decoration: none !important;
    font-size: 12px;
}
.loglink:hover{
    color:#ffffff;
}

.loglink h6{
    font-size: 12px;
}
    
</style>

    <div class="col-sm-2 col-md-3 col-lg-6 col-xl-8 d-none d-md-block">
                    <div class="side-1"></div>
                    <div class="side-1-overlay"></div>
                    <div class="info-side">
                        <a class="loglink" href="{{ url('/') }}" target="blank">
                            <h1>Toyota Gh</h1>
                            <h6>Authorized by the Toyota Manufacturer (Toyota Motors Corporation) </h6>               
                        </a>
                        <div class="main-info">
                            
                            <h1>Our Vision</h1>
                            <h6> To provide customers with the best vehicle purchase and <br>ownership experience. </br></h6>

                            <a href="{{ url('/') }}" target="blank">
                            <button class="btn btn-primary ob-btn btn-light" type="button">Learn more</button><button class="btn btn-primary ob-btn btn-flat" type="button" >Visit Our Page</button>
                            </a>
                        </div>
                        <!-- the span class below has a Home link caption -->
                                
                        <div
                            class="bottom"><span></span><span><a class="loglink" href="{{ url('/') }}"> <?php echo date ('Y') ?> Developed By Abdul Sheriff</a></span>
                        </div>
                    </div>
    </div>
    <div class="col">
        <div class="login-container">
            <img src="{{ url('backend/assets/img/cdd_logo.png') }}" >
            <h6 class="sub-title"><br>Welcome back, please login to your <br>dashboard<br><br></h6>
            <form id="login-form" role="form" method="POST" action="{{ url('/login') }}">

                {{ csrf_field() }}

                @if ($errors->has('email'))
                    <div class="error" align="center">
                        <strong>{{ $errors->first('email') }}</strong>
                    </div>
                @endif

          
                <div class="form-group input-field s12 {{ $errors->has('email') ? ' has-error' : '' }}"><label>Email<br></label>
                    <input class="form-control validate" id="email" type="text" name="email" value="{{ old('email') }}" required autofocus>
                </div>

                @if ($errors->has('password'))
                <span class="error">
                            <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif

                <div class="form-group input-field s12  {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Password</label>
                    <input class="form-control" id="password" type="password" name="password" required>
                </div>
                <div class="col-md-6 col-md-offset-4">
                    <input type="checkbox" id="remember" name="remember" />
                    <label for="remember">Remember Me</label>
                </div>
                <p id="fgt-pass"><a href="{{ url('/password/reset') }}">Forgot Password?</a></p>
                

                <div class="btn-cont"><button class="btn btn-primary ob-btn btn-dark" type="submit">Login </button></div>
            </form>
            <div class="bottom">
                <p>You don't have account? Kindly <span> <a href="{{ url('/register') }}" style="color: red !important; "> Sign Up</a> to proceed</span><br></p>
            </div>

            
        </div>
    </div>






@endsection
























{{--  @extends('layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection  --}}
