








@extends('layouts.auth')

@section('content')

<style type="text/css">
.loglink{
    color: #ffffff;
    text-decoration: none !important;
    font-size: 12px;
}
.loglink:hover{
    color:#ffffff;
}

.loglink h6{
    font-size: 12px;
}
    
</style>

    <div class="col-sm-2 col-md-3 col-lg-6 col-xl-8 d-none d-md-block">
                    <div class="side-2"></div>
                    <div class="side-1-overlay"></div>
                    <div class="info-side">
                        <a class="loglink" href="#" target="blank">
                            <h1>Toyota Gh</h1>
                            <h6>Authorized by the Toyota Manufacturer (Toyota Motors Corporation) </h6>               
                        </a>
                        <div class="main-info">
                            
                            <h1>Our Vision</h1>
                            <h6> To provide its customers with the best vehicle purchase and <br>ownership experience. </br></h6>
                            <a href="#" target="blank">
                            <button class="btn btn-primary ob-btn btn-light" type="button">Learn more</button><button class="btn btn-primary ob-btn btn-flat" type="button" >Visit Our Page</button>
                            </a>
                        </div>
                        <!-- the span class below has a Home link caption -->
                                
                        <div
                            class="bottom"><span></span><span><a class="loglink" href="#"><?php echo date ('Y') ?> Developed By Abdul Sheriff</a></span>
                        </div>
                    </div>
    </div>
    <div class="col">
        <div class="login-container">
          <a href="{{ url('/') }}">  <img src="{{ url('backend/assets/img/cdd_logo.png') }}" ></a>
            {{--  <h6 class="title">Center for Democracy and Development</h6>  --}}
            <h6 class="sub-title"><br>Welcome back, please login to your <br>dashboard<br><br></h6>
            <form class="register-form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class="form-group input-field s12 {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Name</label>

                        
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        
                    </div>

                    <div class="form-group input-field s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">E-Mail Address</label>

                        
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        
                    </div>

                    <div class="form-group input-field s12 {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password</label>

                        
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        
                    </div>

                    <div class="form-group input-field s12">
                        <label for="password-confirm">Confirm Password</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>

                     <div class="btn-cont">
                            <button type="submit" class="btn btn-primary ob-btn btn-dark">
                                Register
                            </button>
                      </div>
                    
                </form>
                <br>
            <div class="bottom">
                
                <p>I already have account? Kindly <span> <a href="{{ url('/login') }}" style="color: red !important; "> Sign In</a> to proceed</span><br></p>
            </div>

            
        </div>
    </div>






@endsection



