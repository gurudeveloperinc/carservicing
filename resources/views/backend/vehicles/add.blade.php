@extends('layouts.admin')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }

        .select-car{
            width: 100%;
            height: 40px;
            border-radius: 3px;
            border-color: gainsboro;
        }
</style>
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
@include('main.top')
</div>

<div class="row mt-5 ml-5">
    <div class="col-xl-8 mb-5 mb-xl-0">
            @include('notification')
            <h3>Add Vehicle Information</h3>
          <div class="card shadow">

            <form method="POST" action="{{ url('vehicle') }}" >
                    {{csrf_field()}}
                <div class="row">
                <div class="col-md-5 mt-5 ml-5">
                    <div class="form-group">
                    <input type="text" name="vehicle_make" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Vehicle Make">
                    </div>
                </div>
                 <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="brand" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Brand">
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-10 ml-5">
                    <div class="form-group">
                            <label for="Category"> Vehicle Category</label>
                            <select class="form-group select-car" name="category" default="Selec category" type="text" required autofocus>
                                    <option value="Car">Car</option>
                                    <option value="SUV">SUV</option>
                                    <option value="VAN">VAN</option>
                                    <option value="Truck">Truck</option>
                                    <option value="Wagon">Wagon</option>
                                    <option value="Others">Others</option>                    
                            </select>
                    </div>
                </div>
                </div>
                <div class="col-md-10 ml-5 mb-3">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <button class="btn btn-danger" type="submit">Cancel</button>
                </div>   
      </form>
     </div>
  </div>
</div>

@endsection 