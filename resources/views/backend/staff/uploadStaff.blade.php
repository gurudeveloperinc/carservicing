@extends('layouts.admin')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }

        .select-car{
            width: 100%;
            height: 50px;
            border-radius: 3px;
            border-color: gainsboro;
        }
</style>
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
@include('main.top')
</div>

<div class="row mt-5 ml-5 mb-10">
    <div class="col-xl-10 mb-10 mb-xl-0">
            @include('notification')
            <h3>Upload Staff Bulk Information</h3>
          <div class="card shadow">

            <form method="POST" action="{{ url('vehicle') }}" >
                    {{csrf_field()}}
                
                <div class="row">
                
                    
                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                            <label>Upload Staff Data</label>
                            <input type="file"  name="image" accept="image/*">
                        </div>
                    </div>
                </div>
               
            
                </div>
                <div class="col-md-10 ml-5 mt-2 mb-3">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <button class="btn btn-danger" type="submit">Cancel</button>
                </div>   
      </form>
     </div>
  </div>
</div>

@endsection 