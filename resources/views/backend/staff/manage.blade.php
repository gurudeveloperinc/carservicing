@extends('layouts.admin')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }

        .select-car{
            width: 100%;
            height: 40px;
            border-radius: 3px;
            border-color: gainsboro;
        }
</style>
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
@include('main.listtop')
</div>


<div class="row mt-5 ml-5">
    <div class="col-xl-8 mb-5 mb-xl-0">
            <h3>Manage Appointments</h3>
     <div class="card shadow">

          
     </div>
  </div>
</div>


<div class="container-fluid mt--7">
    <!-- Table -->
    <div class="row">
      <div class="col">
        <div class="card shadow">
          <div class="card-header border-0">
            <h3 class="mb-0">Home/Staff</h3>
            @include('notification')
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Email</th>
                  <th scope="col">Category</th>
                  <th scope="col">Gender</th>
                  <th scope="col">Photo</th>
                  <th scope="col">Designation</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                  {{--  the foreach was here  --}}

                  @if(count($staff)>0)

                  <?php $count = 1; ?>

                  @foreach($staff as $s)
                  
                <tr>

                  <th scope="row">

                      <div class="media align-items-center">
                          <a href="#" class="avatar rounded-circle mr-3">
                              {{--  the counting numbers below  --}}
                              <?php echo $count;?>   
                          </a>
                      <div class="media-body">
                        <span class="mb-0 text-sm">{{ $s->name }}</span>
                      </div>
                    </div>
                  </th>
                  <td>
              
                    {{$s->email}}     
                 </td>
                  <td>
                    {{ $s->category }}
                  </td>
                  <td>
                    {{ $s->gender }}
                  </td>
                  
                  <td>
                    <div class="avatar-group">
                      <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Ryan Tompson">
                        <img alt="" src="{{ url('/frontend/assets/img/theme/team-1-800x800.jpg') }}" class="rounded-circle">
                      </a>
                    </div>
                  </td> 
                  <td>
                    {{ $s->designation->name }}
                  </td>
                  <td>
                    <div class="d-flex align-items-center">
                            {{--  {{ url('/appointment-details/'.$a->aid) }}  --}}
                      <a href="{{ url('view-staff-detail/'.$s->stid) }}"><button class="progress-bar bg-info">View Detais</button></a>
                      
                    </div>
                  </td>
                  <td class="text-right">
                    <div class="dropdown">
                      <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"></i>
                      </a>
                      <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item" href="{{ url('delete-staff/'.$s->stid) }}">Delete</a>
                       
                        <a class="dropdown-item" href="{{ url('edit-staff/'.$s->stid) }}">Edit</a>
                      </div>
                    </div>
                  </td>
                </tr>

                <?php $count ++; ?>
               
                  @endforeach
                  @else

                      <tr>
                          <td colspan="7">
                              <h3 style="color: silver; text-align: center; margin-top: 30px;"> There are no Staff Record available </h3>
                          </td>
                      </tr>


                  @endif


{{-- The dump was here --}}

              </tbody>
            </table>
          </div>
          <div class="card-footer py-4">
            <nav aria-label="...">
              <ul class="pagination justify-content-end mb-0">
                <li class="page-item disabled">
                  <a class="page-link" href="#" tabindex="-1">
                    <i class="fas fa-angle-left"></i>
                    <span class="sr-only">Previous</span>
                  </a>
                </li>
                <li class="page-item active">
                  <a class="page-link" href="#">1</a>
                </li>
                <li class="page-item">
                  <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                </li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                  <a class="page-link" href="#">
                    <i class="fas fa-angle-right"></i>
                    <span class="sr-only">Next</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- Dark table -->
    
   @include('footer')
  </div>

  
@endsection 