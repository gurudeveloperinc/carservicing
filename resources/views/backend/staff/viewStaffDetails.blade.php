

@extends('layouts.admin')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }

        .select-car{
            width: 100%;
            height: 40px;
            border-radius: 3px;
            border-color: gainsboro;
        }

        .app-details{
            background-color:#ffffff;
        }

        .details-head{
            color: #d0142c; 
        }

      
          #myImg {
            border-radius: 5px;
            cursor: pointer;
            transition: 0.3s;
          }
          
          #myImg:hover {opacity: 0.7;}
          
          /* The Modal (background) */
          .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
          }
          
          /* Modal Content (image) */
          .modal-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
          }
          
          /* Caption of Modal Image */
          #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
          }
          
          /* Add Animation */
          .modal-content, #caption {  
            -webkit-animation-name: zoom;
            -webkit-animation-duration: 0.6s;
            animation-name: zoom;
            animation-duration: 0.6s;
          }
          
          @-webkit-keyframes zoom {
            from {-webkit-transform: scale(0)} 
            to {-webkit-transform: scale(1)}
          }
          
          @keyframes zoom {
            from {transform: scale(0.1)} 
            to {transform: scale(1)}
          }
          
          /* The Close Button */
          .close {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
          }
          
          .close:hover,
          .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
          }
          
          /* 100% Image Width on Smaller Screens */
          @media only screen and (max-width: 700px){
            .modal-content {
              width: 100%;
            }
          }
          
</style>

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
@include('main.top')
</div>

<!-- Stack the columns on mobile by making one full-width and the other half-width -->

<div class="container ml-5 mt-7 app-details">
   <a class="btn btn-primary mt-3" href="{{ url('manage-staff') }}">back</a> 
    <br>
        <h1 class="details-head mt-5 ml-5 mb-5">Staff Details</h1>

<div class="row">

  <div class="col-12 col-md-8"> 
     <img id="myImg" class="ml-5" style="width: 100%; height: auto; max-height:100px; max-width:100px;" src="{{$staff->image}}" alt="Image 150 x 150" /> 

     <!-- The Modal -->
      <div id="myModal" class="modal">
        <span class="close">×</span>
        <img class="modal-content" id="img01">
        <div id="caption"></div>
      </div>
</div>


  <div class="col-6 col-md-4">

    <label for="">Staff ID</label>
    <P>{{ $staff->staffid }}</P>
  </div>
</div>
<hr>
<!-- Columns start at 50% wide on mobile and bump up to 33.3% wide on desktop -->
<div class="row ml-5">
  <div class="col-6 col-md-4">
      <label for="">Name</label>
      <p> {{ $staff->fname }}</p>
  </div>
  <div class="col-6 col-md-4">
      <label for="">Date of Birth</label>
      <p>{{ $staff->dob }}</p>
  </div>
  <div class="col-6 col-md-4">
      <label for="">Email</label>

      <p>{{ $staff->email }}</p>
  </div>
</div>
<hr>
<!-- Columns are always 50% wide, on mobile and desktop -->
<div class="row ml-5">
  <div class="col-6 col-md-4">
    <label for="">Position</label>
    
    <p>{{ $staff->designation->name }}</p>
    
  </div>
  <div class="col-6">
      <label for="">Status</label>
    
      <p>active</p>
  </div>
</div>


<hr>
<div class="card ml-5 mr-5">
        <div class="card-header">
          <h5 class="title">Profile Information</h5>
        </div>
        <div class="card-body">
          <form>
            <div class="row">
              <div class="col-md-5 pr-1">
                <div class="form-group">
                  <label>Residential Address</label>
                  <p>{{ $staff->residentialAddress }}</p>
                </div>
              </div>
              <div class="col-md-3 px-1">
                <div class="form-group">
                  <label>Region</label>
                  <p>{{ $staff->region }}</p>
                </div>
              </div>
              <div class="col-md-4 pl-1">
                <div class="form-group">
                  <label>Phone Number</label>
                  <p>{{ $staff->phone }}</p>
                </div>
              </div>
            </div>
            {{--  <div class="row">
              <div class="col-md-6 pr-1">
                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" class="form-control" placeholder="Company" value="Chet">
                </div>
              </div>
              <div class="col-md-6 pl-1">
                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" class="form-control" placeholder="Last Name" value="Faker">
                </div>
              </div>
            </div>  --}}
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>Date Employed</label>
                  <p>{{ $staff->created_at }}</p>
                </div>
              </div>
            </div>
            
            {{--  <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>About Me</label>
                  <textarea rows="4" cols="80" class="form-control textarea">Oh so, your weak rhyme You doubt I'll bother, reading into it</textarea>
                </div>
              </div>
            </div>  --}}
          </form>
          
        </div>
      </div>
      <br><br>


    </div>
    
    <div class="container-fluid">

    @include('footer')
    </div>

    <script>
      // Get the modal
      var modal = document.getElementById('myModal');
      
      // Get the image and insert it inside the modal - use its "alt" text as a caption
      var img = document.getElementById('myImg');
      var modalImg = document.getElementById("img01");
      var captionText = document.getElementById("caption");
      img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        captionText.innerHTML = this.alt;
      }
      
      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("close")[0];
      
      // When the user clicks on <span> (x), close the modal
      span.onclick = function() { 
        modal.style.display = "none";
      }
      </script>
@endsection 