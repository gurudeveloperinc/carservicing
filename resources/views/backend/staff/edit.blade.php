@extends('layouts.admin')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }

        .select-car{
            width: 100%;
            height: 50px;
            border-radius: 3px;
            border-color: gainsboro;
        }
</style>
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
@include('main.top')
</div>

<div class="row mt-5 ml-5 mb-10">
    <div class="col-xl-10 mb-10 mb-xl-0">
            @include('notification')
            <h3>Add Staff Information</h3>
          <div class="card shadow">

            <form method="POST" action="{{ url('post-edit-staff/'.$staff->stid) }}" >
                    {{csrf_field()}}
                <div class="row">
                <div class="col-md-5 mt-5 ml-5">
                    <div class="form-group">
                    <input type="text" name="title" value="{{ $staff->title }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="title">
                    </div>
                </div>
                 <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="fname" value="{{ $staff->fname }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="fname">
                        </div>
                    </div>
                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="sname" value="{{ $staff->sname }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="sname">
                        </div>
                    </div>
                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="date" name="dob" value="{{  $staff->dob }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="dob" title="Date of Birth">
                        </div>
                    </div>
                    <div class="col-sm-5 ml-5">
                        <div class="form-group">
                                <select class="form-group select-car" name="gender" default="Gender" type="text">
                                        <option>Gender</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                </select>
                        </div>
                    </div>
        
                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="phone" value="{{ $staff->phone }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="phone">
                        </div>
                    </div>

                </div>
                <div class="row">
                <div class="col-sm-5 ml-5">
                    <div class="form-group">
                            <select class="form-group select-car" name="region" default="Select category" type="text">
                                    <option>Select Region</option>
                                    <option>Kumasi</option>
                                    <option>Cape Coast</option>
                                    <option>Eastern Region</option>
                                    <option>Wastern Region</option>
                                    <option>Nothern Region</option>                    
                            </select>
                    </div>
                </div>

                <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="nationality" value="{{ $staff->nationality }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="nationality">
                        </div>
                    </div>
                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="email" name="email" value="{{ $staff->email }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="mail@example.com">
                        </div>
                    </div>
                    
                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                            <label>Upload Staff Photo</label>
                            <input type="file"  name="image" accept="image/*">
                        </div>
                    </div>
                </div>
              
                <div class="row">
                    <div class="col-md-10 ml-5">
                        <div class="form-group">
                            <label for="designation">Select Designation:</label>
                            <select class="btn btn-dropdown dropdown-toggle" aria-labelledby="Designation" name="did" value="{{ old('did') }}">

                            @foreach ($designation as $d )   
                                <option value="{{ $d->did }}">{{ $d->name }}</option>
                            @endforeach
                            
                            </select>
                        </div>
                    </div>

                    <div class="col-md-11 ml-5">
                        <div class="form-group">
                                <textarea name="residentialAddress" class="form-control" id="exampleFormControlTextarea1" rows="3" col="12" placeholder="residential address"> {{ $staff->residentalAddress }}</textarea>
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-md-10 ml-5 mt-2 mb-3">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <a href="{{ url('/manage-staff') }}" style="text-decoration:none;" class="btn btn-danger"> Cancel</a>
                </div>   
      </form>
     </div>
  </div>
</div>

@endsection 