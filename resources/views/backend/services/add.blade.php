@extends('layouts.admin')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }
</style>
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
@include('main.top')
</div>

<div class="row mt-5 ml-5">
    <div class="col-xl-8 mb-5 mb-xl-0">
            @include('notification')
            <h3>Service Description</h3>
          <div class="card shadow">

            <form method="POST" action="{{ url('service') }}" >
                    {{csrf_field()}}
                <div class="row">
                <div class="col-md-10 mt-5 ml-5">
                    <div class="form-group">
                    <input type="text" name="name" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="Name">
                    </div>
                </div>

                <div class="col-md-10 ml-5">
                        <div class="form-group">
                                <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Description"></textarea>
                            </div>
                    </div>
                    
                </div>
                <div class="col-md-10 ml-5 mb-3">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <button class="btn btn-danger" type="submit">Cancel</button>
                </div>   
             

            
      </form>
     </div>
  </div>
</div>

@endsection 