@extends('layouts.app')


@section('content')
<style>
  
  textarea[class=mymessage] {
    resize: none;
    min-width: 100%;
  }
</style>
@include('main.header')



<!--  appointments -->
  <div id="appointments" class="appointment-main-block appointment-two-main-block">
    <div class="container">
      <div class="row">
          @include('notification')
        <div class="section text-center">
          <h3 class="section-heading text-center">Book an Appointments</h3>
          <p class="sub-heading text-center">By filling in the information below </p>
        </div>
        <div class="col-md-4 hidden-sm">
          <div class="appointment-img">
            <img src="{{ url('frontend/images/appointment.jpg') }}" class="img-responsive" alt="Appointment">
          </div>
        </div>
        
        <div class="col-md-8 col-sm-12">
          <div class="appointment-block">
            
            <form id="appointment-form" class="appointment-form" method="post" enctype="multipart/form-data" action="{{url('book-appointment')}}">
                {{csrf_field()}}

              <h5 class="form-heading-title"><span class="form-heading-no">1.</span>Vehicle Information</h5>
              <div class="row">
                <div class="col-sm-4">
                  <div class="dropdown">
                    <select class="btn btn-dropdown dropdown-toggle" aria-labelledby="service-type" name="category" value="{{ old('category') }}">
                            
                        <option value="express_maintenance"><a href="#" >Express Maintenance</a></option>
                        <option value="express_repairs"><a href="#" >Express Repairs</a></option>
                        <option value="heavy_repairs"><a href="#" >Heavy Repairs</a></option>
                    </select>
                  </div>
                </div>
                        
                <div class="col-sm-4">
                  <div class="dropdown">
                    <select class="btn btn-dropdown dropdown-toggle" aria-labelledby="vehical-make" name="vid" value="{{ old('vid') }}">

                      @foreach ($vehicle as $v )   
                        <option value="{{ $v->vid }}">{{ $v->vehicle_make }}</option>
                      @endforeach
                      
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="vehical-model" placeholder="Enter Vehicle Model">
                </div>
              </div>
              <h5 class="form-heading-title"><span class="form-heading-no">2.</span>Appointment Information</h5>
              <div class="row">
                <div class="col-sm-6">
                  <input type="text" class="form-control date-pick" id="appointment-date" name="appointment_date" placeholder="Appointment Date">
                </div>
                <div class="col-sm-6">
                  <div class="dropdown">
                      <select class="btn btn-dropdown dropdown-toggle" aria-labelledby="appointment-time" name="appointment_time">
                          <option><a href="#">Morning</a></option>
                          <option><a href="#">Afternoon</a></option>
                          <option><a href="#">Evening</a></option>
                      </select>
                  </div>
                </div>
              </div>

              
              <h5 class="form-heading-title"><span class="form-heading-no">3.</span>Select Type of Service Needed</h5>
              <div class="row">  
                <div class="col-sm-12">
                    <div class="one-of-two pd-left-15">
                        <div class="preffered-time-wrap">
                            <div class="wrap-checked-all">
                                <div class="wrap-checked">
                                    <input type="checkbox" class="form-check-input" name="service[]" value="airCondition" id="Air">
                                    <label for="Air">Air-conditioning</label>
                                </div>
                                <div class="wrap-checked">
                                    <input type="checkbox" name="service[]" value="wheel" id="Wheel">
                                    <label for="Wheel">Wheel-alignment</label>
                                </div>
                                <div class="wrap-checked">
                                    <input type="checkbox" name="service[]" value="oil filter" id="Oil">
                                    <label for="Oil">Oil-lub-filters</label>
                                </div>
                                <div class="wrap-checked">
                                    <input type="checkbox" name="service[]" value="Heating" id="Heating">
                                    <label for="Heating">Heating-cooling</label>
                                </div>
                                <div class="wrap-checked">
                                    <input type="checkbox" name="service[]" value="Break" id="Brake">
                                    <label for="Brake">Brake-repair</label>
                                </div>
                                <div class="wrap-checked">
                                    <input type="checkbox" name="service[]" value="Steering" id="Steering">
                                    <label for="Steering">Steering-suspension</label>
                                </div>
                                <div class="wrap-checked">
                                    <input type="checkbox" name="service[]" value="Engineen" id="Engine">
                                    <label for="Engine">Engine-diagnostic</label>
                                </div>
                                <div class="wrap-checked">
                                    <input type="checkbox" name="service[]" value="Transmission" id="Transmission">
                                    <label for="Transmission">Transmission-repair</label>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>

              
              {{--  New  --}}
              <h5 class="form-heading-title"><span class="form-heading-no">4.</span>Upload Damage Image (Optional)</h5>
              <div class="row">
                  <input type="file" name="image">
              </div>
              {{--  end of new  --}}

              <h5 class="form-heading-title"><span class="form-heading-no">4.</span>Car Owners Details</h5>
              <div class="row">
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="name" name="name" placeholder="Full Name" required>
                </div>
                <div class="col-sm-4">
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" required>
                </div>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" required>
                </div>
                <div class="col-sm-12">
                  <textarea name="message" class="mymessage" rows="4" cols="110" placeholder="Give Detail Description of your booking (Optional)"></textarea>
                </div>
              </div>

            
              <button type="submit" class="btn btn-default pull-right">Book Now</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--  end appointments -->

@include('main.footer')


@endsection




