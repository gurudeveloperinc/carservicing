@extends('layouts.app')


@section('content')
<style>
  
  textarea[class=mymessage] {
    resize: none;
    min-width: 100%;
  }
</style>
@include('main.header')



<!--  appointments -->
  <div id="appointments" class="appointment-main-block appointment-two-main-block">
    <div class="container">
      <div class="row">
          @include('notification')
        <div class="section text-center">
          <h3 class="section-heading text-center">Register</h3>
          <p class="sub-heading text-center">By filling in the information below </p>
        </div>
        <div class="col-md-4 hidden-sm">
          <div class="appointment-img">
            <img src="{{ url('frontend/images/appointment.jpg') }}" class="img-responsive" alt="Appointment">
          </div>
        </div>
        
        <div class="col-md-8 col-sm-12">
          <div class="appointment-block">
            <form id="appointment-form" class="appointment-form" method="post" enctype="multipart/form-data" 
            action="{{url('post-register-customer')}}">
                {{csrf_field()}}

                
                
              <h5 class="form-heading-title"><span class="form-heading-no">4.</span>Personal Details</h5>
              <div class="row">
                
                <div class="col-sm-12">
                  <div class="dropdown">
                    <select class="btn btn-dropdown dropdown-toggle" aria-labelledby="service-type" name="title" value="{{ old('title') }}">
                            
                        <option><a href="#" >Mr</a></option>
                        <option><a href="#" >Mrs</a></option>
                        <option><a href="#" >Miss</a></option>
                    </select>
                  </div>
                </div>
              </div> 
              <br>     
              <div class="row">
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Full Name" required>
                </div>
                <div class="col-sm-4">
                  <input type="email" class="form-control" id="email" name="email" {{ old('email') }} placeholder="Email Address" required>
                </div>
                <div class="col-sm-4">
                  <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" required>
                </div>
                {{-- <div class="col-sm-4">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" required>
                </div> --}}
                <div class="col-sm-12">
                  <textarea name="address" class="mymessage" rows="4" cols="110" placeholder="Address"></textarea>
                </div>
              </div>

            
              <button type="submit" class="btn btn-default pull-right">Register</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--  end appointments -->

@include('main.footer')


@endsection




