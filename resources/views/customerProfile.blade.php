@extends('layouts.app')


@section('content')
<style>
  
  textarea[class=mymessage] {
    resize: none;
    min-width: 100%;
  }
</style>
@include('main.header')



<!--  appointments -->
  <div id="appointments" class="appointment-main-block appointment-two-main-block">
    <div class="container">
      <div class="row">
          @include('notification')
        <div class="section text-center">
          <h3 class="section-heading text-left">Profile Information</h3>
          <p class="sub-heading text-left">Welcome to Toyota Ghana </p>
        </div>
       
 
        
        <div class="col-md-8 col-sm-12">
          <div class="appointment-block">
            

              <h5 class="form-heading-title"><span class="form-heading-no"><i class="fa fa-user"></i> </span>Profile Information</h5>
              <div class="row">
                <div class="col-sm-4">
                  
                    <p class="form-control">{{ $customer->name}}</p>
                </div>
                <div class="col-sm-4">
                    <p class="form-control">{{ $customer->email}}</p>
                </div>
                <div class="col-sm-4">
                  <p class="form-control">{{ $customer->phone}}</p>
                </div>
                <div class="col-sm-12">
                  
                 @if($customer->address) 
                    <p class="form-control" title="Address Details">{{ $customer->address}}</p>
                    @else
                    <p class="form-control">No Address Specified</p>
                 @endif
                </div>
              </div>

            
              {{-- <button type="submit" class="btn btn-default pull-right">Click to Update</button> --}}

              <a href="{{ url('edit-customer/'.$customer->cuid) }}" class="btn btn-default pull-right">Click to Update</a>
          </div>
        </div>

        <div class="col-md-4 hidden-sm">
                <div class="appointment-img">
                  {{-- <img src="{{ url('frontend/images/appointment.jpg') }}" class="img-responsive" alt="Appointment"> --}}

                  {{-- <button type="submit" class="btn btn-default pull-right">Book Now</button> --}}
                </div>
                <a href="{{ url('/') }}" class="btn btn-primary pull-right" style="border:solid 2px; padding: 5px;"> Change Password</a>
                 
                  {{-- <li><a href="btn btn-default pull-right"> Update Profile</a></li> --}}
        </div>

      </div>
    </div>
  </div>
<!--  end appointments -->

@include('main.footer')


@endsection




