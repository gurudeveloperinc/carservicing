@extends('layouts.admin')

@section('content')


   
    
<!-- ============================================================== -->
<!-- 						Content Start	 						-->
<!-- ============================================================== -->
        
<div class="row page-header">
<div class="col-lg-6 align-self-center ">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/home') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Change Password</li>		
    </ol>
</div>
</div>

<section class="main-content">

<div class="row">
    <div class="col-sm-8 ml-5">
        @include('notification')
        <div class="card">
            <div class="card-header card-default">
                Change Password
            </div>
            <div class="card-body">
                <form method="post" id="form" action="{{ url('/change-password') }}">
                    {{ csrf_field() }}
                    <div class="form-group ">
                        
                        <label for="oldpassword">Old Password</label>

                        <input id="oldpassword" type="password" class="form-control form-control-rounded" name="oldpassword" required>
                    </div>
                
                    <div class="form-group">
                        
                        <label for="password">Password</label>

                        <input id="password" type="password" class="form-control form-control-rounded" name="password" required>
                        
                    </div>

                    <div class="form-group">
                        <label for="confirmpassword">Confirm Password</label>

                        <input id="confirmpassword" type="password" class="form-control form-control-rounded" name="confirmpassword" required>

                        <small class="text-muted">Kindly Click change to change your password for category.</small>

                    </div>
                        
                    <button type="submit" class="btn btn-primary margin-l-5 mx-sm-3">Change</button>
                    
                    
                </form>


            </div>
        </div>
    </div>
</div>
</section>

@endsection