@extends('layouts.app')


@section('content')

<style>
    {{--  .bannerimg{
        width: 200px !important;
    }  --}}

    .bgBanner img{
        position: relative;
        width:100%;
        min-height: 100%;
        
        background-size:cover;
        left:0;
    }
</style>

<!--  navigation -->

<div class="nav-bar navbar-fixed-top">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="logo">
                <a href="{{ url('/') }}"><img src="{{ url('frontend/images/logo.png') }}" alt="logo"></a>
              </div>
            </div>
            <div class="col-md-8">
              <div class="navigation">
                <div id="cssmenu"><div id="menu-button">Menu</div>
                  <ul>
                    <li><a href="#">Home</a></li>                
                    <li><a href="#">About</a></li>
                    <li><a href="#">Services</a></li>                
                    <li><a href="#">Gallery</a></li>
                    
                    
                  
                    @if(session()->has('customer'))
                      <li><a href="{{ url('/book-appointment') }}">Book</a></li>
                    @endif
                    
                    {{-- @guest --}}
                    @if(session()->has('customer'))
                    <li class="has-sub"><span class="submenu-button"></span><a href="#">Account</a>
                      <ul>
                       

                            {{-- @else --}}
                            <li class="has-sub" title="You are Welcome">
                                <a id="navbarDropdown" href="#"  title="You are Welcome">
                                  {{-- {{ substr(Auth::user()->name, 0, 7) }}  --}}

                                
                                  {{session()->get('customer')->name}}
                                  
                                  {{-- {{Auth::user()->fname}}</a> --}}
                                </a>
                            
                        <ul>
                      
                              {{-- <li title="You are Welcome">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" title="You are Welcome">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form> --}}

                                @if(session()->has('customer'))
                               
                                @php(  $user = session()->get('customer'))
                                <li title="You are Welcome">
                                  <a class="dropdown-item" href="{{ url('logout-user/'.$user->role) }}">
                                    <i class="ni ni-ui-04"></i> Logout
                                  </a>
                                </li>
                                @endif
                              </li>
                        </ul>
                        </li>
                        @else

                        <li><a href="{{ url('customer/login') }}">Login</a></li>
                        @if (Route::has('register'))
                        <li><a href="{{ url('register-customer') }}">Register</a></li>
                        @endif


                        @endif


                        @if(session()->has('customer'))
                        @php(  $user = session()->get('customer'))

                        <li><a href="{{ url('/profile/'.$user->cuid) }}">View Profile</a></li>
                        @endif

                    {{-- @endguest --}}


                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </div>        
          </div>
        </div>
      </div>
       
<!--  end navigation -->

{{-- slider --}}
{{-- end of slider --}}
{{--  <img class="bannerimg" src="{{ url('frontend/images/slider-1.jpg') }}" alt="">  --}}

<div class="bgBanner">
        <img style="width:100%; background-size:cover; " src="{{ url('frontend/images/slider-1.jpg') }}" alt="">

</div>

<!--  process -->
  <div id="process" class="process-main-block">
    <div class="container">
      <div class="section text-center">
        <h3 class="section-heading">Our Repair Process</h3>
        <p class="sub-heading">Our repair process follows the following sequence</p>
      </div> 
      <div class="row">
        <div class="col-md-3 col-sm-6">
          <div class="process-block text-center">
            <div class="process-icon">
              <i class="fa fa-phone" aria-hidden="true"></i>
            </div>
            <h5 class="process-heading">Booking</h5>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-6">
          <div class="process-block text-center">
            <div class="process-icon">
              <i class="fa fa-eye" aria-hidden="true"></i>
            </div>
            <h5 class="process-heading">Confirm</h5>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
                <div class="process-block text-center">
                  <div class="process-icon">
                    <i class="fa fa-car" aria-hidden="true"></i>
                  </div>
                  <h5 class="process-heading">Process</h5>
                </div>
              </div>
        <div class="col-md-3 col-sm-6">
          <div class="process-block text-center">
            <div class="process-icon">
              <i class="fa fa-smile-o" aria-hidden="true"></i>
            </div>
            <h5 class="process-heading">Complete</h5>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--  end process -->


<!--  gallery -->
  <div id="work-gallery" class="work-gallery-main-block">
    <div class="parallax" style="background-image: url('frontend/images/bg/work-gallery-bg.jpg')">
    <div class="overlay-bg"></div>
      <div class="container">
        <div class="section text-center">
          <h3 class="section-heading">Working gallery</h3>
          <p class="sub-heading">Our gallery showing list of accomplished services rendered to customers.</p>
        </div>
        <div id="work-gallery-slider" class="work-gallery-slider">
          <div class="item work-gallery-block"> 
            <img src="{{ url('frontend/images/gallery/gallery-01.jpg') }}" class="img-responsive" alt="gallery-01">
            <div class="overlay-bg"><a href="{{ url('frontend/images/gallery/gallery-01.jpg') }}" title="Your Image Title"><i class="fa fa-plus"></i></a></div>
          </div>
          <div class="item work-gallery-block"> 
            <img src="{{ url('frontend/images/gallery/gallery-02.jpg') }}" class="img-responsive" alt="gallery-02">
            <div class="overlay-bg"><a href="{{ url('frontend/images/gallery/gallery-02.jpg') }}" title="Your Image Title"><i class="fa fa-plus"></i></a></div>
          </div>
          <div class="item work-gallery-block"> 
            <img src="{{ url('frontend/images/gallery/gallery-03.jpg') }}" class="img-responsive" alt="gallery-03">
            <div class="overlay-bg"><a href="{{ url('frontend/images/gallery/gallery-03.jpg') }}" title="Your Image Title"><i class="fa fa-plus"></i></a></div>
          </div>
          <div class="item work-gallery-block"> 
            <img src="{{ url('frontend/images/gallery/gallery-04.jpg') }}" class="img-responsive" alt="gallery-04">
            <div class="overlay-bg"><a href="{{ url('frontend/images/gallery/gallery-04.jpg') }}" title="Your Image Title"><i class="fa fa-plus"></i></a></div>
          </div>
          <div class="item work-gallery-block"> 
            <img src="{{ url('frontend/images/gallery/gallery-05.jpg') }}" class="img-responsive" alt="gallery-05">
            <div class="overlay-bg"><a href="{{ url('frontend/images/gallery/gallery-05.jpg') }}" title="Your Image Title"><i class="fa fa-plus"></i></a></div>
          </div>
          <div class="item work-gallery-block"> 
            <img src="{{ url('frontend/images/gallery/gallery-06.jpg') }}" class="img-responsive" alt="gallery-06">
            <div class="overlay-bg"><a href="{{ url('frontend/images/gallery/gallery-06.jpg') }}" title="Your Image Title"><i class="fa fa-plus"></i></a></div>
          </div>
        </div>
      </div>      
    </div>
  </div>
<!--  end gallery -->


<!--  plans -->
  <div id="pricing-plan" class="pricing-plan-main-block">
    <div class="container">
      <div class="section text-center">
        <h3 class="section-heading">Vehicle Categories</h3>
        <p class="sub-heading">We fix different kinds of Toyota vehicles ranging from saloon car to cargo truck</p>
      </div>
      <div class="pricing-plan-tab">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#plan-1" aria-controls="plan-1" role="tab" data-toggle="tab"><span><i class="icon-1"></i></span>Regular Car</a></li>
          <li role="presentation"><a href="#plan-2" aria-controls="plan-2" role="tab" data-toggle="tab"><span><i class="icon-3"></i></span>Medium Car</a></li>
          <li role="presentation"><a href="#plan-3" aria-controls="plan-3" role="tab" data-toggle="tab"><span><i class="icon-2"></i></span>Compact SUV</a></li>
          <li role="presentation"><a href="#plan-4" aria-controls="plan-4" role="tab" data-toggle="tab"><span><i class="icon-4"></i></span>Mini Van</a></li>
          <li role="presentation"><a href="#plan-5" aria-controls="plan-5" role="tab" data-toggle="tab"><span><i class="icon-6"></i></span>Pickup Truck</a></li>
          <li role="presentation"><a href="#plan-6" aria-controls="plan-6" role="tab" data-toggle="tab"><span><i class="icon-8"></i></span>Cargo Truck</a></li>
        </ul>
    </div>
    <!-- Tab panes -->
    <br><br>
    <div class="section text-center">
                <a href="{{ url('/customer/login') }}" class="btn btn-default">Book Now</a>  
    </div>
    </div>
  </div>
<!--  end plans -->


@include('main.footer')
@endsection








{{-- <!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Car Servicing System
                </div>

            </div>
        </div>
    </body>
</html> --}}
