<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_histories', function (Blueprint $table) {
            $table->increments('ahid');
            $table->integer('aid')->nullable();
            $table->integer('uid')->nullable();
            $table->integer('sid')->nullable();
            $table->integer('vid')->nullable();
            $table->enum('category',['express_maintenance','express_repairs','heavy_repairs'])->default('express_maintenance');
            $table->string('appointment_date')->nullable();
            $table->string('appointment_time')->nullable();
            $table->string('name')->nullable();
            $table->string('photo')->nullable();
            $table->enum('status',['pending','processing','completed'])->default('pending');
            $table->string('email')->unique()->nullable();
            $table->string('phone')->nullable();
            $table->string('message',5000)->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_histories');
    }
}
