<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('aid');
            $table->integer('uid');
            $table->integer('sid');
            $table->integer('vid');
            $table->enum('category',['express_maintenance','express_repairs','heavy_repairs'])->default('express_maintenance');
            $table->string('appointment_date');
            $table->string('appointment_time');
            $table->string('name');
            $table->string('photo')->nullable();
            $table->enum('status',['pending','processing','completed'])->default('pending');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('message',5000);
            $table->rememberToken();
            $table->timestamps();
            
        });

        Schema::create('services', function (Blueprint $table) {
            $table->increments('sid')->index();
            $table->integer('aid');
            $table->string('service_types');
            $table->timestamps();

        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('uid');
            $table->integer('stid'); //I need clearification
		    $table->string('name');
		    $table->string('email',191)->unique();
		    $table->string('password');
		    $table->enum('role',['manager','admin','staff','customer'])->default('customer')->nullable();
		    $table->enum('state',['active','in_active'])->default('active');
		    $table->rememberToken();
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
