<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Designation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create( 'roles', function ( Blueprint $table ) {
			$table->increments( 'rlid' );
			$table->string('name');
			$table->softDeletes();
			$table->timestamps();

        } );
        
        Schema::create( 'staff', function ( Blueprint $table ) {
			$table->increments( 'stid' );
			$table->string('staffid')->nullable();
			$table->integer('did');
			$table->integer('uid')->nullable();
			$table->string('title');
			$table->string('fname');
			$table->string('sname');
			$table->string('dob');
			$table->string('image',2000)->nullable();
			$table->enum( 'gender', [ 'male', 'female' ])->default('male');
			$table->string( 'residentialAddress', 2000 );
			$table->string( 'region' );
			$table->string('phone');
			$table->string('nationality');
			$table->string('category')->nullable();
			$table->string('password');
			$table->softDeletes();
			$table->timestamps();

		} );

        Schema::create( 'customers', function ( Blueprint $table ) {
			$table->increments( 'cuid' );
			$table->string('customerid')->nullable();
			$table->integer('uid')->nullable();
			$table->string('title');
			$table->string('name');
			$table->string('email');
			$table->string( 'address', 2000 );
			$table->string('phone');
			$table->string('password');
			$table->softDeletes();
			$table->timestamps();

		} );

        Schema::create( 'designations', function ( Blueprint $table ) {
			$table->increments('did');
			$table->string('category');
			$table->string('name',191 )->unique();
			$table->softDeletes();
			$table->timestamps();

		} );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('roles');
        Schema::dropIfExists('staff');
        Schema::dropIfExists('customers');
        Schema::dropIfExists('designations');

    }
}
