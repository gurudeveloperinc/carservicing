<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentHistory extends Model
{
    //
    protected $table = 'appointment_histories';
    protected $guarded = [];
    protected $primaryKey = 'ahid';
}
