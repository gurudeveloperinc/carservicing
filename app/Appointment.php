<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
    protected $table = 'appointments';
    protected $guarded = [];
    protected $primaryKey = 'aid';

    public function service() {
        return $this->hasMany(service::class,'aid','aid');
    }

    public function vehicle() {
        return $this->hasMany(vehicle::class,'vid','vid');
    }
}
