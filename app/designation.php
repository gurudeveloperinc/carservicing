<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class designation extends Model
{
    //
    protected $table = 'designations';
    

    protected  $primaryKey = 'did';
    protected  $guarded = [ ];

    public function Staff(  ) {
		return $this->hasMany(staff::class,'did','did');
    }

    

}
