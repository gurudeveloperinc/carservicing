<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{
    //
    protected $table = 'customers';
    

    protected  $primaryKey = 'cuid';
    protected  $guarded = [ ];

    
    public function user() {
      return $this->belongsTo(user::class,'uid','uid');
    }
}
