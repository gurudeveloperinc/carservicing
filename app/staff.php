<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class staff extends Model
{
    //
    
    protected $table = 'staff';
    

    protected  $primaryKey = 'stid';
    protected  $guarded = [ ];

    public function Designation(  ) {
		return $this->belongsTo(designation::class,'did');
    }

    public function user() {
      return $this->belongsTo(user::class,'uid','uid');
    }
}
