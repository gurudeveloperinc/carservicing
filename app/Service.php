<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //
    protected $primaryKey = 'sid';
    protected  $table = 'services';

    protected $fillable = [];

    public function appointment() {
        return $this->belongsTo(appointment::class,'aid','aid');
    }
}
