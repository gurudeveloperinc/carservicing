<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    //
    protected $table = 'vehicles';
    protected $guarded = [];
    protected $primaryKey = 'vid';

    public function appointment() {
        return $this->belongsTo(appointment::class,'vid','vid');
    }
}
