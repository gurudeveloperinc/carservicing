<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role == 'admin'){
		    return $next($request);
	    } else {
    		$request->session()->flash('error','Sorry you are not permitted to access that route.');
    		return redirect('/staff/login');
	    }
    }
}
