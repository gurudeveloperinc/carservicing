<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class customer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

    	if($request->session()->has('customer'))
	        return $next($request);
    	else
        	return redirect('customer/login');
        
        // if(Auth::user()->role == 'customer'){
		//     return $next($request);
	    // } else {
    	// 	$request->session()->flash('error','Sorry you are not permitted to access that route.');
    	// 	return redirect('/customer/login');
	    // }
    }
}
