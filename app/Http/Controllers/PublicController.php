<?php

namespace App\Http\Controllers;

use App\Service;
use App\Appointment;
use App\AppointmentHistory;
use App\Vehicle;
use App\User;
use App\staff;
use App\customer;
use App\designation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Faker\Provider\Company;
use http\Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;


class PublicController extends Controller
{
    //
    /*
    //MYTODO:
    
    make it such that when a useris booking for an appointment,
    if the the email and name exist, it should get the user id and add to the apointment table 
    else it should store the the user a default password of 123456 and parhaps email user with this notification
    carrying this user's information. 
	*/
    
    // public function index(){

    // $customer = customer::all();
    // return view('welcome',[
    //     'customer' => $customer
    // ]);

    // }



    
    public function about(){
		return view('frontend.about');

    }

    public function contact(){
        return view('frontend.contact');
    }

    
    // start
    public function registerCustomer(){
        return view('frontend.register');

    }

    public function postRegisterCustomer(Request $request){
        try{

            DB::beginTransaction();
            
           
    
            $customer = new customer();
            $customer->title = $request->input('title');
            $customer->name = $request->input('name');
            $customer->phone = $request->input('phone');
            $customer->email = strtolower($request->input('email'));
            $customer->address = $request->input('residentialAddress');
            $customer->password = bcrypt("Toyotagh123");    
            $customer->save();
    

            $user      		= new User();
            $user->name 	= $customer->name;
            $user->email 	= $customer->email;
            $user->password = $customer->password;
            $user->role 	= 'customer';
            $user->state 	= 'active';
            $user->save();
            
            DB::commit();
            
                // session()->flash('success','Regisgtration Successful');
                // after signing up email the customers password
                
                return redirect('customer/login')->with('success','Regisgtration Successful, Kindly Login to Proceed.');
                
            }catch (\Exception $exception){
    
    
                return $exception->getMessage();
                session()->flash('error',"Something went wrong. Please try again or contact IT.");
    
                return redirect()->back();
            }
    
    }
	// end
	
	


}
