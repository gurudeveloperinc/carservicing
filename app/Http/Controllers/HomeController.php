<?php

namespace App\Http\Controllers;

use App\Service;
use App\customer;
use App\Appointment;
use App\AppointmentHistory;
use App\User;
use App\Vehicle;
use App\staff;
use App\designation;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Faker\Provider\Company;
use http\Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
// testing something
    public function index()
    {
        $pendingAppointments = appointment::where('status','pending')->count();
        $appointment = appointment::where('status','processing')->count();
        $appointments = appointment::where('status','completed')->count();
        $dailyappointments = appointment::whereDate('created_at', Carbon::today())->count();;
        //var_dump($appointments);
        //die($appointments);
        return view('home', 
        
        [
            
            'appointments'          =>  $appointments,
            'appointment'           =>  $appointment,
            'pendingAppointments'   => $pendingAppointments,
            'dailyappointments'     => $dailyappointments
            ]);
    }

// end of testing something

// this is admin dashboard
    // public function dashboard(){
    //     $appointments = appointment::where('status','completed')->count();
    //     $appointment = appointment::where('status','processing');
    //     //sdie((string)$appointments);
    //     return view('home', [
    //     'appointments' =>  $appointments,
    //     'appointment' =>  $appointment
    //     ]);
    // }

    public function dashboard()
    {
        $pendingAppointments = appointment::where('status','pending')->count();
        $appointment = appointment::where('status','processing')->count();
        $appointments = appointment::where('status','completed')->count();
        $staff = staff::all();
        //var_dump($appointments);
        //die($appointments);
        return view('home', 
        
        [
            
            'appointments' =>  $appointments,
            'appointment' =>  $appointment,
            'staff' => $staff,
            'pendingAppointments' => $pendingAppointments
            ]);
    }


    public function getService(){
		return view('backend.services.add');
		
    }
    
    public function postService(Request $request){
        $service = new Service();
		$service->name = $request->input('name');
		$service->description = $request->input('description');

		$status = $service->save();
		if($status){
			session()->flash('success', 'Service Added Successfully');
		}
		else{
			session()->flash('error', 'An error occurred. Please try again');
		}
		return redirect()->back();

    }

    public function getVehicle(){
        return view('backend.vehicles.add');
    }

    public function postVehicle(Request $request){
        $vehicle = new Vehicle();
		$vehicle->vehicle_make = $request->input('vehicle_make');
        $vehicle->brand = $request->input('brand');
        $vehicle->category = "Car";


		$status = $vehicle->save();
		if($status){
			session()->flash('success', 'vehicle Added Successfully');
		}
		else{
			session()->flash('error', 'An error occurred. Please try again');
		}
		return redirect()->back(); 
    }
    
    

    public function manageAppointmentData(){
        if (Input::has('term')){
			$term = Input::get('term');
			$appointments = AppointmentHistory::where('category','like',"%$term%")->paginate(5);
		}else{
			$appointments = AppointmentHistory::orderBy('created_at','desc')->paginate(10);
		}
	    
        return view('backend.appointments.manageAppointentData',[
            'appointments' => $appointments
		  ]);

    }

    public function viewAppointmentDataDetails($ahid){
        $appointments = AppointmentHistory::findorfail($ahid);
        return view('backend.appointments.details', [
            'appointments' => $appointments
        ]);
    }


    public function deleteAppointmentData($ahid){
        $appointment = AppointmentHistory::destroy($ahid);
            if ($appointment){
                session()->flash('success','Deleted successfully');
            }else{
                session()->flash('error','Sorry something went wrong');

            }
            return redirect()->back();
    }

// manage appointments was here



public function manageAppointment(){
    // $appointment = Appointment::all();

    // return view('',[
    //     $appointments => 'appointments'
    // ]);
    // if (Input::has('term')){
    // 	$term = Input::get('term');
    // 	$appointments = Appointment::where('category','like',"%$term%")->paginate(5);
    // }else{}
        
    //     $appointments = Appointment::orderBy('created_at','desc')->paginate(10);
    

    //     // backend.dashboard.staff.appointments.manage
    // return view('backend.appointments.manage',[
    //     'appointments' => $appointments
    //   ]);

}

// appointment details was here
    public function addDesignation( ) {
		return view('backend.staff.designation');
	}

	public function postDesignation(Request $request) {
		$designation = new designation();
		$designation->name = $request->input('name');
		$designation->category = $request->input('category');

		$status = $designation->save();
		if ($status){
			session()->flash('success','Designation Added.');
		}else{
			session()->flash('error', 'Sorry, Something Went Wrong');
		}
		return redirect()->back();

	}

	public function designations() {
		$designation = designation::all();
		return view('designation.manage',[
			'designations' => $designation
		]);
    }
    
    public function editDesignation( $did ) {
		$designation = designation::find($did);

		return view('designation.edit',[
			'designation' => $designation
		]);
	}

    //below is an example of update cascade 
	public function postEditDesignation( Request $request,$did) {

		 $designation = designation::find($did);
		 $designation->name = $request->input('name');
		 $designation->category = $request->input('category');
		 $designation->save();

		 $staff = staff::where('did',$designation->did)->get();

		 foreach($staff as $item){
		 	$thisStaff = staff::find($item->stid);
		 	$thisStaff->category = $designation->category;
		 	$thisStaff->save();
		 }
		 session()->flash('success','Designation Edited');
		 return redirect()->back();

	}


    public function getAddStaff(){
        $designation = designation::all();
        return view('backend.staff.add', [
            'designation' => $designation
        ]);
    }


    public function manageStaff(){
        $staff = staff::orderBy('created_at','desc')->paginate(5);
        $designation = designation::all();
        return view('backend.staff.manage', [
            'staff' => $staff,
            'designation' => $designation
        ]);
        
        return view('backend.staff.manage');

    }

    public function editStaff($stid){
        $staff = staff::findorfail($stid);
        $designation = designation::all();
		return view('backend.staff.edit',[
            'staff' => $staff,
            'designation' => $designation
		]);
    }

    public function postEditStaff(Request $request, $stid){
        
        $staff  =  staff::findorfail($stid);
		$status = $staff->update($request->all());
		// if($request->hasFile('image')){
		// 	$fileName = $request->file('image')->getClientOriginalName();
		// 	$request->file('image')->move('staff/images/',$fileName);
		// 	$imageUrl = url('staff/images/' . $fileName);
		// 	$staff->image = $imageUrl;
		// 	$status = $staff->save();
        // }
        

        if($request->hasFile('image')){
            $destinatonPath = '';
            $filename = '';
    
            $file = Input::file('image');
            $destinationPath = public_path().'staff/images/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
        }else {
            $filename = url('default-image.png');
        }



		if($status)
		    $request->session()->flash('success','Staff Updated');
		else
		    $request->session()->flash('error','Sorry an error occurred');

		// return redirect('/staff/'. $uid . '/edit');
        return redirect('/manage-staff')->with('success' , 'Staff Record Edited successfully');
    }

    public function deleteStaff($stid){
        
        $staff = staff::destroy($stid);
        if ($staff){
            session()->flash('success','Staff Record Deleted successfully');
        }else{
            session()->flash('error','Sorry something went wrong');

        }
        return redirect()->back();
    }



    public function viewStaffDetails($stid){

        // $staff = staff::where('status','active')->paginate(10);
        $staff = staff::findorfail($stid);
		$designation = designation::all();
        return view ('backend.staff.viewStaffDetails',[
            'staff' => $staff,
            'designation' => $designation
        ]);
    }

    public function postAddStaff (Request $request) {
        
        try{

        DB::beginTransaction();
        
        if($request->hasFile('image')){
            $destinatonPath = '';
            $filename = '';
    
            $file = Input::file('image');
            $destinationPath = public_path().'staff/images/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
        }else {
            $filename = url('/images/default-image.png');

        }

        $did = $request->input('did');
        $designation = designation::find($did);

		$staff = new staff();
        $staff->did = $request->input('did');
		$staff->title = $request->input('title');
		$staff->fname = $request->input('fname');
		$staff->sname = $request->input('sname');
		$staff->dob  = $request->input('dob');
		$staff->gender = $request->input('gender');
		$staff->phone = $request->input('phone');
		$staff->email = strtolower($request->input('email'));
		$staff->image = $request->input('image');
		$staff->region = $request->input('region');
		$staff->nationality = $request->input('nationality');
		$staff->residentialAddress = $request->input('residentialAddress');
		$staff->category = $designation->category;
		$staff->password = bcrypt("Toyotagh123");    
        $staff->save();

        $s_fname = str_slug($staff->fname);
        $s_sname = str_slug($staff->sname);

        $fullname = strtoupper($s_fname . " " . $s_sname);
        $user      = new User();
        $user->stid = $staff->stid;
        $user->name = $fullname;
        $user->email = $staff->email;
        $user->password = $staff->password;
        $user->role = 'staff';
        $user->state = 'active';
		$user->save();
        
        DB::commit();
		
            session()->flash('success','Staff Added');
            
            return redirect()->back();
            
        }catch (\Exception $exception){


            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact IT.");

            return redirect()->back();
        }


    }
    

    public function changePassword() {
		return view('changePassword');
	}

	public function postChangePassword( Request $request ) {
		$oldpassword = $request->input('oldpassword');
		$password = $request->input('password');
		$confirmpassword = $request->input('confirmpassword');
		$user = User::find(Auth::user()->uid);

		if(password_verify($oldpassword,$user->password) ){

			if($confirmpassword == $password){
				$user->password = bcrypt($password);
				$request->session()->flash("success","Password changed successfully");
				$user->save();
				return redirect('/home');

			}
			else{
				$request->session()->flash("error", "Both new passwords don't match. Please try again");
				return redirect('/change-password');
			}

		} else {
			$request->session()->flash("error", "Current Password is wrong. Please try again.");
			return redirect('/change-password');
		}
        return redirect('/home')->with('Success', 'Password Changed Successfully');
	}

    public function logout() {
		auth()->logout();
		session()->flash('success',"You have been logged out.");
		return redirect('/');
    }



}
