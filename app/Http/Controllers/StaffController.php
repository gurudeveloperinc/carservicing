<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Service;
use App\Appointment;
use App\AppointmentHistory;
use App\User;
use App\Vehicle;
use App\staff;
use App\customer;
use App\designation;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Faker\Provider\Company;
use http\Exception;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;




class StaffController extends Controller
{
    
    //
    
	public function __construct() {
		$this->middleware('staff');
    }
    

    public function dashboard() {

		// if (session()->has('staff')){
		// 	$stid = session()->get('staff')->stid;
		// 	$this->staffId = $stid;
		// 	$staff = staff::findorfail($stid);
		// 	$subjects = $this->mySubjects();

		// 	$class = $this->myClasses();

        // }
        
        $pendingAppointments = appointment::where('status','pending')->count();
        $appointment = appointment::where('status','processing')->count();
        $appointments = appointment::where('status','completed')->count();
        $customers = customer::all();

        return view('dashboard.staff.dashboard'
         ,[
		// 	'classes' => $class,
		// 	'staff' => $staff,
		// 	'subjects' => $subjects
        
            'appointments' =>  $appointments,
            'appointment' =>  $appointment,
            'pendingAppointments' => $pendingAppointments,
            'customers' => $customers
        ]
    );

	}



// manage appointment was here

public function manageBookings(){
    $appointments = Appointment::orderBy('created_at','desc')->paginate(6);
    
    // backend.dashboard.staff.appointments.manage
    return view('backend.appointments.manage',[
    'appointments' => $appointments
    ]);  
}





    
public function viewAppointmentDetails($aid){
    $appointments = Appointment::findorfail($aid);
    return view('backend.appointments.manageDetails', [
        'appointments' => $appointments
    ]);
}


    
	// public function applicationDetail($apid) {

	// 	$application   = Application::find($apid);
	// 	return view('admission.detail',[
	// 		'application' => $application
	// 	]);
    // }
    

    //Track the level of satisfaction using radio botton and also
    //see if you can display the history transaction for each unique
    // customer below the details 
    //make staff to change the status of the appointment
    // and also let the level of satisfaction be displayed 
    //only when the status is sets to completed
  
    // the appointment details was here

    
    public function processAppointment($aid){
        $appointment = Appointment::find($aid);
		$appointment->status = 'processing';
        $status = $appointment->save();

        
		if ($status){
			session()->flash('success','Request Processing');
		}else{
			session()->flash('error', 'Sorry, Something Went Wrong');
		}
		return redirect()->back();
        //later round when you click process appointment, it should take you to a page where you add small info tellling the customer that the duration it will take for the car to be fixed after keying in all, it should send an email notification to the customer telling the customer that your appointment is processing. 

    }
    public function deleteAppointment(Request $request, $aid){
//I want it to be such that before you delete, it should insert the appointment into appointment_history when a record is deleted it's deleted so you can't delete a record which is deleted. so in that case it the manage appointment page should only display appointment whose status are pending and processing. 
        
        try{

			DB::beginTransaction();
				$appointment = Appointment::find($aid);
				
				$appointmenthistory                    = new AppointmentHistory();
                $appointmenthistory->aid    		   = $appointment->aid;
                $appointmenthistory->uid               = $appointment->uid;
                $appointmenthistory->vid    		   = $appointment->vid;

                // $appointment->email            = $appointment( 'email' );
                $appointmenthistory->category          = $appointment->category;
                $appointmenthistory->appointment_time  = $appointment->appointment_time;
                $appointmenthistory->appointment_date  = $appointment->appointment_date;
                $appointmenthistory->photo   		   = $appointment->photo; 
                $appointmenthistory->status   		   = $appointment->status;
                $appointmenthistory->name              = $appointment->name;
                $appointmenthistory->email			   = $appointment->email;
                $appointmenthistory->phone             = $appointment->phone;
                $appointmenthistory->message          = $appointment->message;
                $appointmenthistory->save();
    
			DB::commit();

            $appointment = Appointment::destroy($aid);
            if ($appointment){
                session()->flash('success','Deleted successfully');
            }else{
                session()->flash('error','Sorry something went wrong');

            }
            return redirect()->back();
        }
        catch(\Exception $exception){
            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact IT.");
            
			return redirect()->back();
        }
    }

    public function completeAppointment(Request $request, $aid){
        // if(Appointment::where('status', 'completed')->where('email', $email)->count() > 0)
      
        // if(Appointment::where('status', 'completed')->where('email', $email)->count() > 0){

        //     $appointment = Appointment::destroy($aid);
        //     if ($appointment){
        //         session()->flash('success','Deleted successfully');
        //     }else{
        //         session()->flash('error','Sorry something went wrong');

        //     }
        //     return redirect()->back();
        // }else {} //else_end
            # code...
        

        try{

			DB::beginTransaction();
				$appointment = Appointment::find($aid);
				$appointment->status = 'completed';
				$appointment->save();

				
				$appointmenthistory                    = new AppointmentHistory();
                $appointmenthistory->aid    		   = $appointment->aid;
                $appointmenthistory->uid               = $appointment->uid;
                $appointmenthistory->vid    		   = $appointment->vid;

                // $appointment->email            = $appointment( 'email' );
                $appointmenthistory->category          = $appointment->category;
                $appointmenthistory->appointment_time  = $appointment->appointment_time;
                $appointmenthistory->appointment_date  = $appointment->appointment_date;
                $appointmenthistory->photo   		   = $appointment->photo; 
                $appointmenthistory->status   		   = $appointment->status;
                $appointmenthistory->name              = $appointment->name;
                $appointmenthistory->email			   = $appointment->email;
                $appointmenthistory->phone             = $appointment->phone;
                $appointmenthistory->message           = $appointment->message;
                $appointmenthistory->save();
    

			DB::commit();

			session()->flash('success','Successful');

			return redirect()->back();


		}catch (\Exception $exception){


			return $exception->getMessage();
			session()->flash('error',"Something went wrong. Please try again or contact IT.");

			return redirect()->back();
        }
        
      
    }
    

    public function checkFeedback(){
        $hello = 'Hi this is staff';
        echo $hello;
    }
}
