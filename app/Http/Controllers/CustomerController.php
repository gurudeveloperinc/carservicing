<?php

namespace App\Http\Controllers;

// models and relevant class libraries
use App\Appointment;
use App\AppointmentHistory;
use App\Service;
use App\Vehicle;
use App\User;
use App\customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Validator;



class CustomerController extends Controller
{
    
    //
    /*
    //MYTODO:
    
    make it such that when a useris booking for an appointment,
    if the the email and name exist, it should get the user id and add to the apointment table 
    else it should store the the user a default password of 123456 and parhaps email user with this notification
    carrying this user's information. 
	*/
	

	public function __construct() {
		$this->middleware('customer');

	}
    
	public function dashboard() {

		// if (session()->has('staff')){
		// 	$stid = session()->get('staff')->stid;
		// 	$this->staffId = $stid;
		// 	$staff = staff::findorfail($stid);
		// 	$subjects = $this->mySubjects();

		// 	$class = $this->myClasses();

		// }

        return view('welcome'
        // ,[
		// 	'classes' => $class,
		// 	'staff' => $staff,
		// 	'subjects' => $subjects
        // ]
    );

	}


    public function bookAppointment(){
        $service = Service::all(); //TO Load all the services we have in the system
        $vehicle = Vehicle::all();
        return view('frontend.bookAppointment',[
            'service' => $service,
            'vehicle' => $vehicle
		  ]);
    }

    public function postBookAppointment(Request $request){

//	  	return $request->all();
	    $date = $request->input('appointment_date');

	    $appointments = Appointment::where('appointment_date',$date)->get();
	    if(count($appointments) >= 1){
	    	session()->flash('error','Sorry we are all booked for '. $date);
	    	return redirect()->back();
	    }
	    try {
			// if($request->hasFile('image')){
			// 	$filename = $request->file('image')->getClientOriginalName();
			// 	$request->file('image')->move('uploads',Carbon::now()->timestamp . $filename);
			// 	$imageUrl = url('uploads/' . Carbon::now()->timestamp. $filename);
			// } else {
			// 	$imageUrl = url('default-image.png');
			// }
		
	
			if($request->hasFile('image')){
				$destinatonPath = '';
				$filename = '';
		
				$file = Input::file('image');
				$destinationPath = public_path().'/images/';
				$filename = str_random(6).'_'.$file->getClientOriginalName();
				$uploadSuccess = $file->move($destinationPath, $filename);
			}else {
				$filename = url('default-image.png');
			}

			$cuid = session()->get('customer')->cuid;
			// Auth::user()->uid;

		    $appointment                   = new Appointment();
		    $appointment->uid              = $cuid;
		    $appointment->vid    		   = $request->input( 'vid' );
		    $appointment->category         = $request->input( 'category' );
		    $appointment->appointment_time = $request->input( 'appointment_time' );
			$appointment->appointment_date = $request->input( 'appointment_date' );
			$appointment->photo 		   = '/images/' . $filename; // $imageUrl;
		    $appointment->name             = $request->input( 'name' );
			// $appointment->email            = $request->input( 'email' );
			$appointment->email			   = strtolower($request->input('email'));
		    $appointment->phone            = $request->input( 'phone' );
		    $appointment->message          = $request->input( 'message' );
		    $appointment->save();

		    $services = $request->input( 'service' );
		    foreach ( $services as $item ) {
			    $service                = new Service();
			    $service->aid           = $appointment->aid;
			    $service->service_types = $item;
			    $service->save();
		    }

		    session()->flash('success','Appointment Submitted, Thank You.');
		    return redirect()->back();



	    }catch (\Exception $exception){
			session()->flash('error','Appointment Not Submitted, Please Try again, Thank You.');
		    return redirect()->back();


		}
		
		// Todo: add status of appointment 
		//staff logsin and see's pending appointment 
		// And clicks processsing  
		// when appointment is completed, user clicks satisfied or something 
		// make the flash message to disaapear after a given seconds with javascript 


    }


	
	
    public function getProfile($cuid){		
		// return view ('customerProfile');
		
		// $customer = customer::all();
        $customer = customer::findorfail($cuid);

		return view('customerProfile',[
			'customer' => $customer
		]);
	}
	
	public function editCustomer($cuid){
        $customer = customer::findorfail($cuid);
		return view('editProfile',[
			'customer' => $customer
		]);
    }

    public function postEditCustomer(Request $request, $cuid){
        
        $customer  =  customer::findorfail($cuid);
		$status = $customer->update($request->all());
		if($status)
		// return redirect('/customer/'. $uid . '/edit');
			return redirect('/profile/'.$customer->cuid)->with('success','Profile Updated Successfully');
		else			
			return redirect()->back()->with('error','Something Went Wrong');
    }


	
	
	public function getFeedback(){
		return view('frontend.feedback');

    }
    public function postFeedback(Request $request){

	}
	

}
