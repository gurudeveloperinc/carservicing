-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 25, 2019 at 05:39 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cardb`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `aid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `category` enum('express_maintenance','express_repairs','heavy_repairs') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'express_maintenance',
  `appointment_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appointment_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sid` int(11) DEFAULT NULL,
  `vid` int(11) NOT NULL,
  PRIMARY KEY (`aid`),
  UNIQUE KEY `appointments_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`aid`, `uid`, `category`, `appointment_date`, `appointment_time`, `name`, `email`, `phone`, `message`, `remember_token`, `created_at`, `updated_at`, `sid`, `vid`) VALUES
(7, 1, 'express_maintenance', '28/02/2019', 'Morning', 'another test', 'parley@ship.com', '08120522261', 'vdfsgsdfd', NULL, '2019-02-25 10:04:21', '2019-02-25 10:04:21', NULL, 0),
(9, 1, 'express_maintenance', '28/02/2019', 'Morning', 'asar sunny', 'ring2rings93@gmail.com', '08120522261', 'sads', NULL, '2019-02-25 10:43:09', '2019-02-25 10:43:09', NULL, 0),
(10, 1, 'express_maintenance', '25/02/2019', 'Afternoon', 'jonah rimamchirika', 'jonahrchirika@gmail.com', '09071752006', 'helloisdsdsd', NULL, '2019-02-25 15:28:40', '2019-02-25 15:28:40', NULL, 0),
(11, 1, 'heavy_repairs', '01/03/2019', 'Morning', 'musa', 'amostimothy7@gmail.com', '08171624968', 'Please help me fix my phone.', NULL, '2019-02-25 15:44:56', '2019-02-25 15:44:56', NULL, 0),
(15, 1, 'express_maintenance', '11/04/2019', 'Morning', 'danladi kifasi', 'dankifasi@gmail.com', '08065544992', 'Neat car please', NULL, '2019-02-25 16:48:35', '2019-02-25 16:48:35', NULL, 1),
(17, 1, 'express_repairs', '27/02/2019', 'Morning', 'jonah rimamchirika', 'abc@gmail.com', '09071752006', 'yjhjhjhjh', NULL, '2019-02-25 17:16:38', '2019-02-25 17:16:38', NULL, 2),
(18, 1, 'express_maintenance', '13/04/2019', 'Afternoon', 'Elijah Ahamadu', 'theworldofelijah@gmail.com', '08087829830', 'Please fix my engine for me thanks.', NULL, '2019-02-25 17:33:29', '2019-02-25 17:33:29', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_02_18_234433_create_vehicles_table', 1),
(4, '2019_02_25_154938_add_to_services_table', 2),
(5, '2019_02_25_163634_add_to_appointments_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `sid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `aid` int(11) NOT NULL,
  `service_types` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`sid`),
  KEY `services_sid_index` (`sid`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`sid`, `created_at`, `updated_at`, `aid`, `service_types`) VALUES
(1, '2019-02-25 16:48:35', '2019-02-25 16:48:35', 15, 'oil filter'),
(2, '2019-02-25 16:48:35', '2019-02-25 16:48:35', 15, 'Steering'),
(3, '2019-02-25 17:16:38', '2019-02-25 17:16:38', 17, 'airCondition'),
(4, '2019-02-25 17:16:38', '2019-02-25 17:16:38', 17, 'wheel'),
(5, '2019-02-25 17:33:29', '2019-02-25 17:33:29', 18, 'wheel'),
(6, '2019-02-25 17:33:29', '2019-02-25 17:33:29', 18, 'Heating'),
(7, '2019-02-25 17:33:29', '2019-02-25 17:33:29', 18, 'Steering'),
(8, '2019-02-25 17:33:29', '2019-02-25 17:33:29', 18, 'Engineen');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('Manager','Admin','Staff','Customer') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Customer',
  `state` enum('active','in_active') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `name`, `email`, `password`, `role`, `state`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jonah Rimamchirika', 'jonahrchirika@gmail.com', '$2y$10$l8JKBJV4/RTiZMJSKfC3OuJNx1ktVgcs/3PcMa22aeyV8hyNQypLG', 'Customer', 'active', 'TmaOtsHGxqJjgBfLumImOZJGxPMHn9PZfhfNhfy1mdDYxd0WCCPCdhGC3VT1', '2019-02-25 15:03:05', '2019-02-25 15:03:05');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
  `vid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vehicle_make` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` enum('SUV','Car','VAN','Truck','Wagon','Others') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Car',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`vid`, `vehicle_make`, `brand`, `category`, `created_at`, `updated_at`) VALUES
(1, 'Toyota FJ Crusier', 'FJ', 'Car', '2019-02-25 15:23:33', '2019-02-25 15:23:33'),
(2, 'Toyota Camry', 'Camry', 'Car', '2019-02-25 15:23:54', '2019-02-25 15:23:54');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
